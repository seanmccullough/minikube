# Provision a k3s cluster using Ansible on Odroid XU4 Linux

Huge shoutout to [Vincent Rabah (IT Wars)](https://github.com/itwars/k3s-ansible) for the core playbooks used in this repo.

This set of ansible playbooks allows you to deploy k3s to a cluster of Odroid XU4-style devices using Ansible.

In my case, I use a stack of Odroid HC1's which have an integrated SATA interface to give support for persistent volumes and stateful sets.

## Prerequisites

You will need to set up trusted SSH key auth to all of your nodes, and edit the `hosts.ini` file to point to your nodes.

## Usage

To deploy the k3s cluster
```
ansible-playbook site.yml
```

To set up physical persistent volumes, edit `persistent_volume.yml` to meet your desired setup.
```
ansible-playbook persistent_volume.yml
```

## TODO

- [ ] Create playbook for provisioning sata disks
- [ ] Improve persistent volume playbook to be driven from hosts.ini